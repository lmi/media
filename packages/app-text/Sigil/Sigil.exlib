# Copyright 2011-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=user-none ] cmake
require python [ min_versions="3.4" blacklist="2" multibuild=false ]

export_exlib_phases src_prepare

SUMMARY="Sigil is a multi-platform F/OSS WYSIWYG epub ebook editor"
DESCRIPTION="
Uses Qt5 or Qt6.
Full Unicode support: everything you see in Sigil is in UTF-16
Full EPUB spec support
Multiple Views: Book View, Code View and Split View
Metadata editor with full support for all possible metadata entries (more than 200) with full descriptions for each
Table Of Contents editor
Multi-level TOC support
Book View fully supports the display of any XHTML document possible under the OPS spec
SVG support
Basic XPGT support
Advanced automatic conversion of all imported documents to Unicode
Currently imports TXT, HTML and EPUB files; more will be added with time
"
HOMEPAGE="https://sigil-ebook.com/"
LICENCES="GPL-3"
SLOT="0"
MYOPTIONS="
    ( providers: qt5 qt6 ) [[ number-selected = exactly-one ]]
"

QT5_MIN_VER="5.10.0"
QT6_MIN_VER="6.2.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        app-spell/hunspell:=
        dev-libs/boost
        dev-libs/pcre2
        dev-python/lxml[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        sys-libs/zlib
        providers:qt5? (
            x11-libs/qtbase:5[>=${QT5_MIN_VER}]
            x11-libs/qttools:5[>=${QT5_MIN_VER}]
            x11-libs/qtwebengine:5[>=${QT5_MIN_VER}]
        )
        providers:qt6? (
            x11-libs/qtbase:6[>=${QT6_MIN_VER}]
            x11-libs/qt5compat:6[>=${QT6_MIN_VER}]
            x11-libs/qttools:6[>=${QT6_MIN_VER}]
            x11-libs/qtwebengine:6[>=${QT6_MIN_VER}]
        )
"

Sigil_src_prepare() {
    cmake_src_prepare

    edo sed -e "/find_package(PythonInterp/ s/3.4/$(python_get_abi)/" \
            -e "/find_package (PythonLibs/ s/3.4/$(python_get_abi)/" \
            -i CMakeLists.txt
}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DSHARE_INSTALL_PREFIX=/usr
    -DDISABLE_UPDATE_CHECK=1
    -DUSE_SYSTEM_LIBS=true
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'providers:qt6 USE_QT6'
)

