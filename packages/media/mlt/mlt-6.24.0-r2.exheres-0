# Copyright 2009-2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=mltframework release=v${PV} suffix=tar.gz ] \
    lua [ multibuild=false with_opt=true ] \
    ffmpeg [ abis=[ 4 ] with_opt=true ] \
    python [ blacklist=none multibuild=false with_opt=true ] \
    alternatives flag-o-matic

SUMMARY="Multimedia authoring and processing framework and a video playout server for television broadcasting"
DESCRIPTION="
MLT is a multimedia framework designed for television broadcasting. As such, it
provides a pluggable architecture for the inclusion of new audio/video sources,
filters, transitions and playback devices.
"
HOMEPAGE+=" https://www.mltframework.org"

UPSTREAM_RELEASE_NOTES="https://www.mltframework.org/blog/v${PV}_released/"

LICENCES="LGPL-2.1 GPL-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
SWIG_LANGUAGES="java lua mono perl php python ruby tcl"
MYOPTIONS="doc ffmpeg gtk jack libsamplerate opengl ogg qt5 sdl sox
    ${SWIG_LANGUAGES}
    frei0r [[ description = [ Adds support for frei0r video effects plugins ] ]]
    php    [[ description = [ Adds support/bindings for the PHP language ] ]]
    plus   [[ description = [ Adds support for additional filters ] ]]
    rubberband [[ description = [ Add support for audio pitch-shifting ] ]]
    swfdec [[ description = [ Adds support for Flash files ] ]]
    ( platform: amd64 x86 )
    ( x86_cpu_features: mmx sse sse2 )
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? ( app-doc/doxygen )
    build+run:
        dev-libs/libxml2:2.0[>=2.5]
        sys-sound/alsa-lib
        frei0r? ( media-plugins/frei0r-plugins )
        gtk? (
            media-libs/libexif
            x11-libs/gdk-pixbuf:2.0
            x11-libs/pango
        )
        jack? (
            dev-libs/glib:2
            media-libs/ladspa-sdk
            media-sound/jack-audio-connection-kit
        )
        java? (
            dev-lang/swig
            virtual/jdk:=
        )
        libsamplerate? ( media-libs/libsamplerate[>=0.1.5] )
        lua? ( dev-lang/swig )
        mono? (
            dev-lang/mono
            dev-lang/swig
        )
        ogg? ( media-libs/libvorbis )
        opengl? (
            media-libs/movit
            x11-dri/mesa
            x11-libs/libX11
        )
        perl? (
            dev-lang/perl:=[>=5&<6]
            dev-lang/swig
        )
        php? (
            dev-lang/php[>=5&<6]
            dev-lang/swig
        )
        plus? (
            media-libs/libebur128
            sci-libs/fftw[>=3]
        )
        python? ( dev-lang/swig )
        qt5? (
            media-libs/libexif
            sci-libs/fftw[>=3]
            x11-libs/libX11
            x11-libs/qtbase:5
            x11-libs/qtsvg:5
        )
        rubberband? ( media-libs/rubberband )
        ruby? (
            dev-lang/ruby:=
            dev-lang/swig
        )
        sdl? (
            media-libs/SDL:2[X]
            x11-libs/libX11
        )
        sox? ( media-sound/sox )
        swfdec? ( media-libs/swfdec:= )
        tcl? (
            dev-lang/swig
            dev-lang/tcl
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-Use-env-var-PKG_CONFIG-instead-of-hard-coding-pkg-co.patch
    "${FILES}"/${PN}-0.6.2-java-includes.patch
    "${FILES}"/${PN}-0.8.8-lua-FLAGS.patch
)

pkg_setup() {
    ffmpeg_pkg_setup

    if [[ $(exhost --target) == *-musl* ]] ; then
        append-flags -DHAVE_STRTOD_L=1 -DHAVE_LOCALE_H=1
    fi
}

src_configure() {
    local language swig_languages=() myconf=()
    for language in ${SWIG_LANGUAGES}; do
        if option "${language}"; then
            case "${language}" in
                mono) swig_languages+=( csharp ) ;;
                   *) swig_languages+=( ${language} ) ;;
            esac
        fi
    done

    if option lua ; then
        edo sed -e "s/--libs lua/--libs lua-$(lua_get_abi)/" \
                -i src/swig/lua/build
    fi

    if option platform:amd64; then
        myconf+=(
            --enable-mmx
            --enable-sse
            --enable-sse2
        )
    elif option platform:x86; then
        myconf+=(
            $(option_enable x86_cpu_features:mmx)
            $(option_enable x86_cpu_features:sse)
            $(option_enable x86_cpu_features:sse2)
        )
    fi

    if option qt5; then
        myconf+=(
            --qt-includedir=/usr/$(exhost --target)/include/qt5
            --qt-libdir=/usr/$(exhost --target)/lib
            $(option_enable qt5 qt)
        )
    else
        myconf+=(
            --disable-qt
        )
    fi

    # If we want to slot mlt one day --enable-extra-versioning may become handy.
    myconf+=(
        --enable-gpl
        --enable-gpl3
        --enable-xml
        --disable-debug
        --disable-lumas
        --disable-ndi
        --disable-opencv
        # Microsoft produced adaptive wide-band speech codec, needs PulseAudio
        --disable-rtaudio
        --disable-sdl
        --disable-vid.stab
        --disable-xine
        --swig-languages="${swig_languages[*]:-none}"
        --without-kde
        --avformat-no-vdpau
        $(option_enable ffmpeg avformat)
        $(option_enable frei0r)
        $(option_enable gtk gdk)
        $(option_enable jack jackrack)
        $(option_enable libsamplerate resample)
        $(option_enable ogg vorbis)
        $(option_enable opengl)
        $(option_enable plus)
        $(option_enable rubberband)
        $(option_enable sdl sdl2)
        $(option_enable sox)
        $(option_enable swfdec)
    )

    econf "${myconf[@]}"
}

src_compile() {
    option python && eval "$(eclectic python script --sh 2)"

    # config.mak is generated in the configure phase
    edo sed -i -e '/^OPTIMISATIONS/d' config.mak
    default
    option doc && edo doxygen Doxyfile
}

src_install() {
    default

    alternatives_for mlt ${SLOT} ${SLOT} \
        /usr/$(exhost --target)/bin/melt melt-${SLOT}

    dodoc docs/*.txt
    option doc && dodoc -r docs/html
    if option java; then
        edo cd "${WORK}"/src/swig/java
        exeinto /usr/share/${PN}/lib
        doexe libmlt_java.so
    fi
    if option lua; then
        edo cd "${WORK}"/src/swig/lua
        lua_install_cmodule mlt.so
    fi
    if option mono; then
        edo cd "${WORK}"/src/swig/csharp
        exeinto /usr/$(exhost --target)/lib
        doexe libmltsharp.so mlt-sharp.dll
    fi
    if option perl; then
        edo cd "${WORK}"/src/swig/perl
        default
    fi
    if option php; then
        edo cd "${WORK}"/src/swig/php
        exeinto "$(php-config --extension-dir)"
        doexe mlt.so
    fi
    if option python; then
        edo cd "${WORK}"/src/swig/python
        exeinto "$(python_get_sitedir)"
        doexe _mlt.so
        insinto "$(python_get_sitedir)"
        doins mlt.py
        python_bytecompile
    fi
    if option ruby; then
        edo cd "${WORK}"/src/swig/ruby
        default
    fi
    if option tcl; then
        edo cd "${WORK}"/src/swig/tcl
        exeinto /usr/$(exhost --target)/lib/${PNV}
        doexe mlt.so
    fi
}

