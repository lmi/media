# Copyright 2010 Jonathan Dahan <jedahan@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson [ meson_minimum_version=0.53.0 ]

SUMMARY="An audio time-stretching and pitch-shifting library and utility program"
HOMEPAGE="https://breakfastquay.com/rubberband"
DOWNLOADS="https://breakfastquay.com/files/releases/${PNV}.tar.bz2"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
    build+run:
        media-libs/ladspa-sdk [[ note = [ could be optional ] ]]
        media-libs/libsamplerate[>=0.1.8] [[ note = [ could be optional ] ]]
        media-libs/libsndfile[>=1.0.16] [[ note = [ could be optional ] ]]
        media-libs/vamp-plugin-sdk[>=2.9] [[ note = [ could be optional ] ]]
        sci-libs/fftw[>=3.0.0]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dfft=fftw
    -Dresampler=libsamplerate
)

src_prepare() {
    meson_src_prepare

    # Doesn't seem to detect jni.h from openjdk-bin, but err on the safe side
    edo sed \
        -e 's/\(have_jni = \).*/\1false/' \
        -i meson.build

}

