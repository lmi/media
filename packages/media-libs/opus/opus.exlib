# Copyright 2012 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require meson

SUMMARY="The Opus Codec incorporates technology from Skype's SILK codec and Xiph.Org's CELT codec"
DESCRIPTION="
The Opus codec is designed for interactive speech and audio transmission over the Internet. It is
designed by the IETF Codec Working Group and incorporates technology from Skype's SILK codec and
Xiph.Org's CELT codec.

The Opus codec is designed to handle a wide range of interactive audio applications, including Voice
over IP, videoconferencing, in-game chat, and even remote live music performances. It can scale from
low bit-rate narrowband speech to very high quality stereo music.
"
HOMEPAGE="https://www.opus-codec.org"
DOWNLOADS="https://downloads.xiph.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( app-doc/doxygen[dot] )
    build+run:
        (
            !media-libs/opus:0.9
            !media-libs/opus:1.0
        ) [[
            *description = [ Old non-working slot scheme ]
            *resolution = uninstall-blocked-after
        ]]
"

MESON_SRC_CONFIGURE_PARAMS=(
    -Dasm=disabled
    -Dassertions=false
    -Dcheck-asm=false
    -Dcustom-modes=true
    -Ddocdir=/usr/share/doc/${PNVR}
    -Dextra-programs=disabled
    -Dfixed-point=false
    -Dfixed-point-debug=false
    -Dfloat-api=true
    -Dfloat-approx=false
    -Dfuzzing=false
    -Dhardening=true
    -Dintrinsics=enabled
    -Drtcd=enabled
)

MESON_SRC_CONFIGURE_OPTION_FEATURES=(
    'doc docs'
)

MESON_SRC_CONFIGURE_TESTS=(
    '-Dtests=enabled -Dtests=disabled'
)

