# Copyright 2011 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require sourceforge [ project=lcms suffix=tar.gz ]
require meson

SUMMARY="A C implementation of ICC 4.3"
DESCRIPTION="
Little CMS intends to be a small-footprint color management engine, with special focus on accuracy
and performance. It uses the International Color Consortium standard (ICC), which is the modern
standard when regarding to color management. The ICC specification is widely used and is referred
to in many International and other de-facto standards. It was approved as an International
Standard, ISO 15076-1, in 2005.
"
HOMEPAGE+=" https://www.littlecms.com"

LICENCES="
    MIT
    GPL-3 [[ note = [ fastfloat/multi threaded plugin ] ]]
"
SLOT="0"
PLATFORMS="~amd64 ~arm ~armv7 ~armv8 ~x86"
MYOPTIONS="
    tiff
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        providers:ijg-jpeg? ( media-libs/jpeg:=[>=6b] )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        tiff? ( media-libs/tiff:= )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-meson-Install-psicc-man-page.patch
)

MESON_SRC_CONFIGURE_PARAMS=(
    -Dfastfloat=true
    -Djpeg=enabled
    -Dthreaded=true
)
MESON_SRC_CONFIGURE_OPTION_FEATURES=( tiff )

