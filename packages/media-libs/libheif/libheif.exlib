# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=strukturag release=v${PV} suffix=tar.gz ]
require cmake

export_exlib_phases src_install

SUMMARY="An image file format employing HEVC (h.265) image coding"
DESCRIPTION="
libheif makes use of libde265 for the actual image decoding and x265 for
encoding. Alternative codecs for, e.g., AVC and JPEG can be provided as
plugins.
"

LICENCES="
    GPL-3  [[ note = [ sample applications ] ]]
    LGPL-3 [[ note = library ]]
"
SLOT="0"
MYOPTIONS="
    aom [[ description = [ Use aom for en-/decoding AVIF images ] ]]
    rav1e [[ description = [ Use rav1e for encoding AVIF images ] ]]
    svt-av1 [[ description = [ Use SVT-AV1 for encoding AVIF images ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"
DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        media-libs/dav1d
        media-libs/libde265
        media-libs/libpng:=
        media-libs/x265:=
        aom? ( media-libs/aom:= )
        rav1e? ( media-video/rav1e )
        svt-av1? ( media-libs/SVT-AV1 )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DENABLE_MULTITHREADING_SUPPORT:BOOL=TRUE
    -DWITH_DAV1D:BOOL=TRUE
    -DWITH_EXAMPLES:BOOL=TRUE
    -DWITH_FUZZERS:BOOL=OFF
    -DWITH_GDK_PIXBUF:BOOL=FALSE
    # decoder for HEIF
    -DWITH_LIBDE265:BOOL=TRUE
    -DWITH_LIBSHARPYUV:BOOL=FALSE
    # encoder for HEIF
    -DWITH_X265:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    # en-/decoder for AVIF
    'aom AOM_DECODER'
    'aom AOM_ENCODER'
    # encoder for AVIF
    RAV1E
    'svt-av1 SvtEnc'
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
    # A bit unfortunate, but "Tests can only be compiled with full symbol
    # visibility"
    '-DWITH_REDUCED_VISIBILITY:BOOL=FALSE -DWITH_REDUCED_VISIBILITY:BOOL=TRUE'
)

CMAKE_SRC_TEST_PARAMS=(
    # Needs -DWITH_UNCOMPRESSED_CODEC:BOOL=TRUE to pass, which is experimental
    -E uncompressed_decode
)

libheif_src_install() {
    cmake_src_install

    # libheif expects this, even if empty
    keepdir /usr/$(exhost --target)/lib/libheif
}

