Source/Upstream: Yes, fixed in SDL-1.2 branch
Reason: Fix build with recent FreeType versions

From c5f9522df2c7305e094cc8ca132414f405f9d04d Mon Sep 17 00:00:00 2001
From: Ozkan Sezer <sezeroz@gmail.com>
Date: Wed, 24 Oct 2018 14:37:40 +0300
Subject: [PATCH] update freetype2 detection to use both pkg-config and
 freetype-config.

closes https://bugzilla.libsdl.org/show_bug.cgi?id=2324
also backport default/2.0 commit 3b93536d291a for opengl test.
---
 Makefile.in            |   7 +-
 acinclude/freetype2.m4 | 194 +++++++++
 aclocal.m4             |   1 +
 configure.in           |  44 +-
 4 files changed, 1001 insertions(+), 139 deletions(-)
 create mode 100644 acinclude/freetype2.m4

diff --git a/Makefile.in b/Makefile.in
index 2ddad54..f9ba9d2 100644
--- a/Makefile.in
+++ b/Makefile.in
@@ -48,7 +48,8 @@ DIST_COMMON = README $(am__configure_deps) \
 	$(srcdir)/SDL_ttf.spec.in $(top_srcdir)/configure COPYING \
 	config.guess config.sub depcomp install-sh ltmain.sh missing
 ACLOCAL_M4 = $(top_srcdir)/aclocal.m4
-am__aclocal_m4_deps = $(top_srcdir)/acinclude/libtool.m4 \
+am__aclocal_m4_deps = $(top_srcdir)/acinclude/freetype2.m4 \
+	$(top_srcdir)/acinclude/libtool.m4 \
 	$(top_srcdir)/acinclude/ltoptions.m4 \
 	$(top_srcdir)/acinclude/ltsugar.m4 \
 	$(top_srcdir)/acinclude/ltversion.m4 \
@@ -155,7 +156,9 @@ ECHO_T = @ECHO_T@
 EGREP = @EGREP@
 EXEEXT = @EXEEXT@
 FGREP = @FGREP@
-FREETYPE_CONFIG = @FREETYPE_CONFIG@
+FT2_CFLAGS = @FT2_CFLAGS@
+FT2_CONFIG = @FT2_CONFIG@
+FT2_LIBS = @FT2_LIBS@
 GL_LIBS = @GL_LIBS@
 GREP = @GREP@
 INSTALL = @INSTALL@
diff --git a/acinclude/freetype2.m4 b/acinclude/freetype2.m4
new file mode 100644
index 0000000..af2e659
--- /dev/null
+++ b/acinclude/freetype2.m4
@@ -0,0 +1,194 @@
+# Configure paths for FreeType2
+# Marcelo Magallon 2001-10-26, based on gtk.m4 by Owen Taylor
+#
+# Copyright 2001-2018 by
+# David Turner, Robert Wilhelm, and Werner Lemberg.
+#
+# This file is part of the FreeType project, and may only be used, modified,
+# and distributed under the terms of the FreeType project license,
+# LICENSE.TXT.  By continuing to use, modify, or distribute this file you
+# indicate that you have read the license and understand and accept it
+# fully.
+#
+# As a special exception to the FreeType project license, this file may be
+# distributed as part of a program that contains a configuration script
+# generated by Autoconf, under the same distribution terms as the rest of
+# that program.
+#
+# serial 4
+
+# AC_CHECK_FT2([MINIMUM-VERSION [, ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND]]])
+# Test for FreeType 2, and define FT2_CFLAGS and FT2_LIBS.
+# MINIMUM-VERSION is what libtool reports; the default is `7.0.1' (this is
+# FreeType 2.0.4).
+#
+AC_DEFUN([AC_CHECK_FT2],
+  [# Get the cflags and libraries from the freetype-config script
+   #
+   AC_ARG_WITH([ft-prefix],
+     dnl don't quote AS_HELP_STRING!
+     AS_HELP_STRING([--with-ft-prefix=PREFIX],
+                    [Prefix where FreeType is installed (optional)]),
+     [ft_config_prefix="$withval"],
+     [ft_config_prefix=""])
+
+   AC_ARG_WITH([ft-exec-prefix],
+     dnl don't quote AS_HELP_STRING!
+     AS_HELP_STRING([--with-ft-exec-prefix=PREFIX],
+                    [Exec prefix where FreeType is installed (optional)]),
+     [ft_config_exec_prefix="$withval"],
+     [ft_config_exec_prefix=""])
+
+   AC_ARG_ENABLE([freetypetest],
+     dnl don't quote AS_HELP_STRING!
+     AS_HELP_STRING([--disable-freetypetest],
+                    [Do not try to compile and run a test FreeType program]),
+     [],
+     [enable_fttest=yes])
+
+   if test x$ft_config_exec_prefix != x ; then
+     ft_config_args="$ft_config_args --exec-prefix=$ft_config_exec_prefix"
+     if test x${FT2_CONFIG+set} != xset ; then
+       FT2_CONFIG=$ft_config_exec_prefix/bin/freetype-config
+     fi
+   fi
+
+   if test x$ft_config_prefix != x ; then
+     ft_config_args="$ft_config_args --prefix=$ft_config_prefix"
+     if test x${FT2_CONFIG+set} != xset ; then
+       FT2_CONFIG=$ft_config_prefix/bin/freetype-config
+     fi
+   fi
+
+   if test "x$FT2_CONFIG" = x ; then
+     AC_PATH_TOOL([FT2_CONFIG], [freetype-config], [no])
+   fi
+
+   min_ft_version=m4_if([$1], [], [7.0.1], [$1])
+   AC_MSG_CHECKING([for FreeType -- version >= $min_ft_version])
+   no_ft=""
+   if test "$FT2_CONFIG" = "no" ; then
+     no_ft=yes
+   else
+     FT2_CFLAGS=`$FT2_CONFIG $ft_config_args --cflags`
+     FT2_LIBS=`$FT2_CONFIG $ft_config_args --libs`
+     ft_config_major_version=`$FT2_CONFIG $ft_config_args --version | \
+       sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
+     ft_config_minor_version=`$FT2_CONFIG $ft_config_args --version | \
+       sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
+     ft_config_micro_version=`$FT2_CONFIG $ft_config_args --version | \
+       sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
+     ft_min_major_version=`echo $min_ft_version | \
+       sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
+     ft_min_minor_version=`echo $min_ft_version | \
+       sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
+     ft_min_micro_version=`echo $min_ft_version | \
+       sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
+     if test x$enable_fttest = xyes ; then
+       ft_config_is_lt=""
+       if test $ft_config_major_version -lt $ft_min_major_version ; then
+         ft_config_is_lt=yes
+       else
+         if test $ft_config_major_version -eq $ft_min_major_version ; then
+           if test $ft_config_minor_version -lt $ft_min_minor_version ; then
+             ft_config_is_lt=yes
+           else
+             if test $ft_config_minor_version -eq $ft_min_minor_version ; then
+               if test $ft_config_micro_version -lt $ft_min_micro_version ; then
+                 ft_config_is_lt=yes
+               fi
+             fi
+           fi
+         fi
+       fi
+       if test x$ft_config_is_lt = xyes ; then
+         no_ft=yes
+       else
+         ac_save_CFLAGS="$CFLAGS"
+         ac_save_LIBS="$LIBS"
+         CFLAGS="$CFLAGS $FT2_CFLAGS"
+         LIBS="$FT2_LIBS $LIBS"
+
+         #
+         # Sanity checks for the results of freetype-config to some extent.
+         #
+         AC_RUN_IFELSE([
+             AC_LANG_SOURCE([[
+
+#include <ft2build.h>
+#include FT_FREETYPE_H
+#include <stdio.h>
+#include <stdlib.h>
+
+int
+main()
+{
+  FT_Library library;
+  FT_Error  error;
+
+  error = FT_Init_FreeType(&library);
+
+  if (error)
+    return 1;
+  else
+  {
+    FT_Done_FreeType(library);
+    return 0;
+  }
+}
+
+             ]])
+           ],
+           [],
+           [no_ft=yes],
+           [echo $ECHO_N "cross compiling; assuming OK... $ECHO_C"])
+
+         CFLAGS="$ac_save_CFLAGS"
+         LIBS="$ac_save_LIBS"
+       fi             # test $ft_config_version -lt $ft_min_version
+     fi               # test x$enable_fttest = xyes
+   fi                 # test "$FT2_CONFIG" = "no"
+
+   if test x$no_ft = x ; then
+     AC_MSG_RESULT([yes])
+     m4_if([$2], [], [:], [$2])
+   else
+     AC_MSG_RESULT([no])
+     if test "$FT2_CONFIG" = "no" ; then
+       AC_MSG_WARN([
+
+  The freetype-config script installed by FreeType 2 could not be found.
+  If FreeType 2 was installed in PREFIX, make sure PREFIX/bin is in
+  your path, or set the FT2_CONFIG environment variable to the
+  full path to freetype-config.
+       ])
+     else
+       if test x$ft_config_is_lt = xyes ; then
+         AC_MSG_WARN([
+
+  Your installed version of the FreeType 2 library is too old.
+  If you have different versions of FreeType 2, make sure that
+  correct values for --with-ft-prefix or --with-ft-exec-prefix
+  are used, or set the FT2_CONFIG environment variable to the
+  full path to freetype-config.
+         ])
+       else
+         AC_MSG_WARN([
+
+  The FreeType test program failed to run.  If your system uses
+  shared libraries and they are installed outside the normal
+  system library path, make sure the variable LD_LIBRARY_PATH
+  (or whatever is appropriate for your system) is correctly set.
+         ])
+       fi
+     fi
+
+     FT2_CFLAGS=""
+     FT2_LIBS=""
+     m4_if([$3], [], [:], [$3])
+   fi
+
+   AC_SUBST([FT2_CFLAGS])
+   AC_SUBST([FT2_LIBS])])
+
+# end of freetype2.m4
diff --git a/aclocal.m4 b/aclocal.m4
index d8532ec..c31b711 100644
--- a/aclocal.m4
+++ b/aclocal.m4
@@ -949,6 +949,7 @@ AC_SUBST([am__tar])
 AC_SUBST([am__untar])
 ]) # _AM_PROG_TAR
 
+m4_include([acinclude/freetype2.m4])
 m4_include([acinclude/libtool.m4])
 m4_include([acinclude/ltoptions.m4])
 m4_include([acinclude/ltsugar.m4])
diff --git a/configure.in b/configure.in
index c209619..e1a0762 100644
--- a/configure.in
+++ b/configure.in
@@ -97,38 +97,13 @@ esac
 AM_CONDITIONAL(USE_VERSION_RC, test x$use_version_rc = xtrue)
 
 dnl Check for the FreeType 2 library
-dnl
-dnl Get the cflags and libraries from the freetype-config script
-dnl
-AC_ARG_WITH(freetype-prefix,[  --with-freetype-prefix=PFX   Prefix where FREETYPE is 
-installed (optional)],
-            freetype_prefix="$withval", freetype_prefix="")
-AC_ARG_WITH(freetype-exec-prefix,[  --with-freetype-exec-prefix=PFX Exec prefix 
-where FREETYPE is installed (optional)],
-            freetype_exec_prefix="$withval", freetype_exec_prefix="")
-
-if test x$freetype_exec_prefix != x ; then
-     freetype_args="$freetype_args --exec-prefix=$freetype_exec_prefix"
-     if test x${FREETYPE_CONFIG+set} != xset ; then
-        FREETYPE_CONFIG=$freetype_exec_prefix/bin/freetype-config
-     fi
-fi
-if test x$freetype_prefix != x ; then
-     freetype_args="$freetype_args --prefix=$freetype_prefix"
-     if test x${FREETYPE_CONFIG+set} != xset ; then
-        FREETYPE_CONFIG=$freetype_prefix/bin/freetype-config
-     fi
-fi
-AC_PATH_PROG(FREETYPE_CONFIG, freetype-config, no)
-no_freetype=""
-if test "$FREETYPE_CONFIG" = "no" ; then
-    AC_MSG_ERROR([
-*** Unable to find FreeType2 library (http://www.freetype.org/)
+PKG_CHECK_MODULES([FT2], [freetype2 >= 7.0.1], [], [dnl
+    AC_CHECK_FT2(,,[AC_MSG_ERROR([dnl
+*** Unable to find FreeType2 library (http://www.freetype.org/)])]
+    )
 ])
-else
-    CFLAGS="$CFLAGS `$FREETYPE_CONFIG $freetypeconf_args --cflags`"
-    LIBS="$LIBS `$FREETYPE_CONFIG $freetypeconf_args --libs`"
-fi
+CFLAGS="$CFLAGS $FT2_CFLAGS"
+LIBS="$LIBS $FT2_LIBS"
 
 dnl Check for SDL
 SDL_VERSION=1.2.4
@@ -174,14 +149,17 @@ case "$host" in
 esac
 AC_MSG_CHECKING(for OpenGL support)
 have_opengl=no
-AC_TRY_COMPILE([
+save_LIBS="$LIBS"
+LIBS="$LIBS $SYS_GL_LIBS"
+AC_TRY_LINK([
  #include "SDL_opengl.h"
 ],[
- GLuint texture;
+ glOrtho( -2.0, 2.0, -2.0, 2.0, -20.0, 20.0 );
 ],[
 have_opengl=yes
 ])
 AC_MSG_RESULT($have_opengl)
+LIBS="$save_LIBS"
 if test x$have_opengl = xyes; then
     CFLAGS="$CFLAGS -DHAVE_OPENGL"
     GL_LIBS="$SYS_GL_LIBS"
