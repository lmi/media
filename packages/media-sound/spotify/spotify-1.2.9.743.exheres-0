# Copyright 2011 Anders Ladegaard Marchsteiner <alm.anma@gmail.com>
# Copyright 2012 Lasse Brun <bruners@gmail.com>
# Distributed under the terms of the GNU General Public License v2

MY_REPO="https://repository-origin.spotify.com/pool/non-free/s/spotify-client"
MY_PN="${PN}-client"
MY_PV="${PV}.g85d9593d"

require gtk-icon-cache freedesktop-desktop

SUMMARY="Music streaming service"
DESCRIPTION="
A DRM-based music streaming service offering streaming of selected music from a range of major and
independent record labels, including Sony, EMI, Warner Music Group, and Universal.
"
HOMEPAGE="https://www.spotify.com"
DOWNLOADS="
    listed-only:
        platform:amd64? ( ${MY_REPO}/${MY_PN}_${MY_PV}_amd64.deb )
"

LICENCES="Spotify"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="
    ( platform: amd64 )
"

RESTRICT="strip"

DEPENDENCIES="
    build:
        dev-util/patchelf
    run:
        dev-libs/at-spi2-atk
        dev-libs/at-spi2-core
        dev-libs/atk
        dev-libs/expat
        dev-libs/glib:2
        dev-libs/nspr
        dev-libs/nss
        net-misc/curl
        sys-apps/dbus
        sys-libs/zlib
        sys-sound/alsa-lib
        x11-dri/libdrm
        x11-dri/mesa
        x11-libs/cairo[X(+)]
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/harfbuzz
        x11-libs/libICE
        x11-libs/libSM
        x11-libs/libX11
        x11-libs/libxcb
        x11-libs/libXcomposite
        x11-libs/libXdamage
        x11-libs/libXext
        x11-libs/libXfixes
        x11-libs/libxkbcommon
        x11-libs/libXrandr
        x11-libs/pango
    suggestion:
        media/ffmpeg:* [[
            description = [ Optional for playback of local files ]
        ]]
        net-apps/NetworkManager [[
            description = [ Optional for unknown feature (dbus.freedesktop.org related) ]
        ]]
        net-print/cups [[
            description = [ Optional for unknown feature, should be for printing ...  ]
        ]]
"

WORK=${WORKBASE}

pkg_setup() {
    exdirectory --allow /opt
}

src_unpack() {
    default

    unpack ./data.tar.gz
    edo rm -rf usr/share/doc
}

src_prepare() {
    # links against libcurl-gnutls.so.4 instead of libcurl.so.4, last checked: 1.0.80.480
    edo patchelf --replace-needed libcurl-gnutls.so.4 libcurl.so.4 usr/share/${PN}/${PN}
}

src_test() {
    :
}

src_install() {
    # icons
    for size in 16 22 24 32 48 64 128 256 512 ; do
        insinto /usr/share/icons/hicolor/${size}x${size}/apps
        newins usr/share/${PN}/icons/${PN}-linux-${size}.png ${PN}.png
    done

    # desktop entry
    insinto /usr/share/applications
    doins usr/share/${PN}/${PN}.desktop
    edo rm usr/share/${PN}/${PN}.desktop

    # everything else
    insinto /opt
    doins -r usr/share/${PN}

    # binary
    edo chmod 0755 "${IMAGE}"/opt/${PN}/${PN}
    dodir /usr/$(exhost --target)/bin
    dosym /opt/${PN}/${PN} /usr/$(exhost --target)/bin/${PN}
}

pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

