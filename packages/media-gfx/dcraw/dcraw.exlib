# Copyright 2011 Xavier Barrachina <xabarci@doctor.upv.es>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'hilite-1.5.exheres-0', which is:
#     Copyright 2007 Bo Ørsted Andresen

export_exlib_phases src_compile src_install

SUMMARY="Command line tool for decoding raw image data from digital cameras"
HOMEPAGE="https://www.cybercom.net/~dcoffin/${PN}"
DOWNLOADS="${HOMEPAGE}/archive/${PNV}.tar.gz"

LICENCES="GPL-2"
SLOT="0"
LANGUAGES=( ca cs da de eo es fr hu it ja nl pl pt ro ru sv zh_CN zh_TW )
MYOPTIONS="
    lcms [[ description = [ Support color profiles ] ]]
    jpeg2000 [[ description = [ Decode Red camera movies using jasper lib ] ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
    build+run:
        lcms? ( media-libs/lcms2 )
        jpeg2000? ( media-libs/jasper )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

WORK=${WORKBASE}/${PN}

dcraw_src_compile() {
    edo ${CC} ${CFLAGS} ${LDFLAGS} -o dcraw dcraw.c -lm -ljpeg \
        -DLOCALEDIR="\"/usr/share/locale\"" \
        $(option jpeg2000 "-ljasper" "-DNO_JASPER") \
        $(option lcms "-llcms2" "-DNO_LCMS")

    for i in "${LANGUAGES[@]}"; do
        edo msgfmt -c -o dcraw_${i}.mo dcraw_${i}.po
    done
}

dcraw_src_install() {
    dobin dcraw
    doman dcraw.1

    for i in "${LANGUAGES[@]}"; do
        insinto /usr/share/locale/${i}/LC_MESSAGES
        newins dcraw_${i}.mo dcraw.mo

        [[ -f dcraw_${i}.1 ]] && \
            insinto /usr/share/man/${i}/man1 && \
            newins dcraw_${i}.1 dcraw.1
    done
}

